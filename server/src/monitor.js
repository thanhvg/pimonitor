const request = require('request');
const { getConfig, addLog } = require('./store.js');
const { toggleRelay } = require('./toggleRelay.js');

// on new config die
const listen = () => {
  console.log('starting ');
  const myConfig = getConfig();

  const uri = `http://${
    myConfig.ipAddress
  }:8080/monitor/camera_info?camera_id=${myConfig.cameraId}`;

  console.log(uri);

  const timeout = myConfig.timeout
    ? myConfig.timeout * 60 * 1000
    : 10 * 60 * 1000;

  request({ method: 'GET', uri }, function(error, response) {
    if (error) {
      if (myConfig.isEnabled) {
        console.log('ERROR GO KILL');
        addLog('Error from HVR, activate relay');
        toggleRelay().then(addLog).catch(addLog);
      } else {
        console.log('ERROR NO KILL, Not enabled');
        addLog('Error from HVR, no relay action, not enabled');
      }
      setTimeout(listen, timeout);
      return;
    }

    if (response.statusCode !== 200 && myConfig.isEnabled) {
      console.log('GO KILL');
      addLog('Error from HVR, activate relay');
      toggleRelay().then(addLog).catch(addLog);
    } else if (response.statusCode === 200) {
      addLog('HVR is working fine, no relay action');
    } else {
      addLog('HVR is not working, no relay action');
    }
    setTimeout(listen, timeout);
  });
};

const check = ({ ipAddress, cameraId }) => {
  const uri = `http://${ipAddress}:8080/monitor/camera_info?camera_id=${cameraId}`;
  return new Promise((resolve, reject) => {
    request({ method: 'GET', uri }, function(error) {
      if (error) {
        reject(new Error('Config saved, but cannot connect to HVR with this IP and camera ID'));
      }
      resolve();
    });
  });
};
module.exports = { listen, check };
