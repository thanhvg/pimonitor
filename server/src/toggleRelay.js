const exec = require('child_process').exec;
const path = require('path');

const relayScript = path.join(__dirname, '../../python/relay.py');


const toggleRelay = () => {
 return new Promise((resolve, reject) => {
   exec(`python2 ${relayScript}`, (err, stdout, stderr) => {
     if (err) {
       reject(err.toString());
       return;
     }
     resolve(stdout + stderr);
   });
   
 }); 
};

module.exports = { toggleRelay };
