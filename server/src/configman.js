const fs = require('fs');
const path = require('path');

const configFile = path.join(__dirname, '../config.json');

const getInitConfig = () => {
  let rawdata = fs.readFileSync(configFile);  
  return JSON.parse(rawdata);  
};

const saveConfig = (data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(configFile, data, (err) => {  
      if (err) {
        reject(err);
        return;
      } 
      resolve();
    });
  }); 
};


module.exports = { getInitConfig, saveConfig };
