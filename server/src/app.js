const express = require('express');
const path = require('path');
const EventEmitter = require('events');
const { listen, check } = require('./monitor.js');
const { getInitConfig, saveConfig, } = require('./configman.js');
const { setConfig, getConfig, getLog } = require('./store.js');
// const event = new event();

const app = express();

const eventEmitter = new EventEmitter();

let savedconfig = getInitConfig();
console.log(savedconfig);
setConfig(savedconfig);

const clientBuildPath = '../../client/build';

app.use(express.static(path.join(__dirname, clientBuildPath)));
app.use(express.json());
app.use(express.urlencoded({ extended: false, }));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, clientBuildPath, 'index.html'));
});

app.get('/api/turnoff', (req, res) => {
  res.json({me: 'hi'});
});

app.get('/api/log', (req, res) => {
  res.json(getLog());
});

app.get('/api/config', (req, res) => {
  res.json(getConfig());
});

app.post('/api/config', async (req, res) => {
  const newConfig = req.body;
  console.log(newConfig);
  try {
    await saveConfig(JSON.stringify(newConfig));
    setConfig(newConfig);
    await check(newConfig);
    res.sendStatus(200);
  } catch (err) {
    res.status(409).json({ error: err.toString() });
  }
});

listen(getConfig(), eventEmitter);

app.listen(8080);
