let config;
let logs = [];
const logLenght = 100;

const setConfig = cfg => {
  config = cfg;
};

const getConfig = () => {
  return { ...config };
};

const addLog = item => {
  if (logs.length > logLenght) {
    logs.shift();
  }
  logs.push(`${new Date().toString()}: ${item}`);
};

const getLog = () => logs;

module.exports = { setConfig, getConfig, addLog, getLog };
