clone
git clone git@jiraserver:/git/pimonitor

or
git clone https://gitlab.com/thanhvg/pimonitor

project structure
.
├── client
│   ├── build
│   ├── node_modules
│   ├── package.json
│   ├── public
│   ├── README.md
│   ├── src
│   └── yarn.lock
├── python
│   └── relay.py
├── Readme.txt
├── run.sh
├── server
│   ├── config.json
│   ├── node_modules
│   ├── package.json
│   ├── src
│   └── yarn.lock
└── TODOs.org

set up node

sudo install nodejs
sudo install yarn

sudo npm install -g yarn

to build from project root
cd client
yarn install
yarn build

cd ../server
yarn install

to run
from root project
sh run.sh


You need to update the python code at
python/relay.py


open web
htttp://[pi_address]]:8080
