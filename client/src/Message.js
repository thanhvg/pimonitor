import React from "react";

class Message extends React.Component {
  render() {
    const { message, onClick } = this.props;

    if (!message) {
      return null;
    }
    return (
      <div className="alert success">
        <span className="closebtn" onClick={onClick}>&times;</span> 
        <strong>Info:</strong> {message}
      </div>
      
    );
  }
}

export default Message;
