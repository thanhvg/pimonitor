import React from "react";

class Error extends React.Component {
  render() {
    const { error, onClick } = this.props;

    if (!error) {
      return null;
    }
    return (
      <div className="alert">
        <span className="closebtn" onClick={onClick}>&times;</span> 
        <strong>Error!</strong> {error}
      </div>
      
    );
  }
}

export default Error;
