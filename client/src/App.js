import React from "react";
import "./App.css";
import Error from './Error.js'
import Message from './Message.js'

class App extends React.Component {
  state = {
    isEnabled: '',
    ipAddress: '',
    cameraId: '',
    timeout: '',
    loading: true,
    error: '',
    message: '',
    log: [],
  };

  handleClick = () => {
    fetch("api/turnoff")
      .then(a => a.json())
      .then(re => {
        console.log(re);
        this.setState({ text: re });
      });
  };

  handleErrorClick = () => {
    this.setState({ error: '' })
  }
  handleMessageClick = () => {
    this.setState({ message: '' })
  }

  getLog = () => {
    fetch("api/log")
      .then(re => re.json())
      .then(log =>
            this.setState({log})
           ).catch(err => console.error);
  }

  handleChange = event => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  componentDidMount() {
    fetch("api/config")
      .then(re => re.json())
      .then(config =>
        this.setState({ ...this.state, ...config, loading: false })
      );
    this.getLog()
  }

  handleSubmit = event => {
    event.preventDefault();
    const { isEnabled, ipAddress, cameraId, timeout } = this.state;
    this.setState({ loading: true, message: '', error: '' })
    fetch("api/config", {
      method: "post",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ isEnabled, ipAddress, cameraId, timeout })
    }).then((res) => {
      console.log(res.status);
      if (res.status === 200) {
        this.setState({ loading: false, message: 'Settings Applied' })
        return;
      }
      return res.json().then(({ error = 'Server error'}) => {
        this.setState( { loading: false, message: '', error})
      });
    }).catch(error => {
      this.setState( { loading: false, message: '', error})
    });
  };

  render() {
    if (this.state.loading) {
      return <div className="App">Loading</div>;
    }
    return (
      <div className="App">
        <div className="header">
          <h1>
            IVT Watchdog 
          </h1>
        </div>
        <div className="content">
          <h2>Settings</h2>
          <form onSubmit={this.handleSubmit}>
            <div>
              <label>
                <input
                  type="checkbox"
                  checked={this.state.isEnabled}
                  name="isEnabled"
                  onChange={this.handleChange}
                />
                Enable Watchdog
              </label>
            </div>
            <div>
              <label>
                <input
                  type="text"
                  value={this.state.ipAddress}
                  onChange={this.handleChange}
                  name="ipAddress"
                />
                HVR IP Address:
              </label>
            </div>
            <div>
              <label>
                <input
                  type="number"
                  value={this.state.cameraId}
                  name="cameraId"
                  onChange={this.handleChange}
                />
                Camara ID to check:
              </label>
            </div>
            <div>
              <label>
                <input
                  type="number"
                  value={this.state.timeout}
                  name="timeout"
                  onChange={this.handleChange}
                />
                Timeout (min):
              </label>
            </div>
            <Error error={this.state.error} onClick={this.handleErrorClick}/>
            <Message message={this.state.message} onClick={this.handleMessageClick}/>
            <input type="submit" value="Apply" />
          </form>

          <h2>Server Log </h2>
          <textarea value={this.state.log.join('\n')} readOnly>
          </textarea>
          <br/>

          <button onClick={this.getLog}>
            Refress
          </button>
        </div>
      </div>
    );
  }
}

export default App;
